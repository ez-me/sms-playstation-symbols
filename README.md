# sms playstation symbols

This is a custom texture pack for Super Mario Sunshine for the GameCube that replaces the buttom prompts to the PlayStation ones, since I'm more used to them.

## How to install

Download the latest release zip and copy the GMS folder on Dolphin/User/Load/Textures.

## Attribution

Thanks to Nicolae "Xelu" Berbece for making their awesome [free controller promtps](https://thoseawesomeguys.com/prompts/) that I used for the FLUDD button prompt.

